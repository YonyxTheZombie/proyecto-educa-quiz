from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError
import random
from django.shortcuts import get_object_or_404, redirect, render
from django.forms.widgets import RadioSelect, Textarea
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from classroom.models import (Answer, Quiz ,Question,Dominio , PlanificacionIndicador,Student, StudentAnswer, Planificacion,
                              Subject,Curso, User,  CorreosJefe,CategoryManager, Category, Colegios, Teacher, Fundacion, Gestion, Jefe_Depto, Niveles, TextoQuestion,
                              Preguntas)
from .custom_layout_object import *


class TeacherSignUpForm(UserCreationForm):

    materias = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True, help_text="Debe ingresar el correo dado por la fundación o el colegio.")
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)


    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        if not "@bostoneduca" and "iom" in email:
            raise ValidationError("Ingrese un correo institucional.")
        return email


    class Meta:
        model = User
        fields = [
                'username',
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2'

        ]
        labels = {
            'username': 'Rut',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_teacher = True
        user.save()
        teacher = Teacher.objects.create(user=user)
        teacher.colegio.add(*self.cleaned_data.get('colegio'))
        teacher.interests.add(*self.cleaned_data.get('materias'))

        teacher.email = self.cleaned_data["email"]
        teacher.password1 = self.cleaned_data["password1"]

        return user


class FundacionSignUpForm(UserCreationForm):
    materias = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    curso = forms.ModelMultipleChoiceField(
        queryset=Curso.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True,help_text="Debe ingresar el correo dado por la fundación o el colegio.")

    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)


    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        if not "@bostoneduca" in email:
            raise ValidationError("Ingrese un correo institucional.")
        return email

    class Meta(UserCreationForm.Meta):
        model = User

        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'

        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }
    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_fundacion = True
        user.save()
        fundacion = Fundacion.objects.create(user=user)
        fundacion.curso.add(*self.cleaned_data.get('curso'))
        fundacion.interests.add(*self.cleaned_data.get('materias'))
        fundacion.colegio.add(*self.cleaned_data.get('colegio'))
        fundacion.email = self.cleaned_data["email"]
        fundacion.password1 = self.cleaned_data["password1"]
        return user


class GestionSignUpForm(UserCreationForm):
    materias = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    curso = forms.ModelMultipleChoiceField(
        queryset=Curso.objects.all(),
        widget=forms.CheckboxSelectMultiple,

        required=True
    )

    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    email = forms.EmailField(label="Correo Institucional", required=True,help_text="Debe ingresar el correo dado por la fundación o el colegio.")

    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)


    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        if not "@bostoneduca" in email:
            raise ValidationError("Ingrese un correo institucional.")
        return email

    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'

        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }
    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_gestion = True
        user.save()
        gestion = Gestion.objects.create(user=user)
        gestion.curso.add(*self.cleaned_data.get('curso'))
        gestion.interests.add(*self.cleaned_data.get('materias'))
        gestion.colegio.add(*self.cleaned_data.get('colegio'))
        gestion.email = self.cleaned_data["email"]
        gestion.password1 = self.cleaned_data["password1"]
        return user


class JefeDSignUpForm(UserCreationForm):
    materias = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    curso = forms.ModelMultipleChoiceField(
        queryset=Curso.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True,help_text="Debe ingresar el correo dado por la fundación o el colegio.")
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)


    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        if not "@bostoneduca" in email:
            raise ValidationError("Ingrese un correo institucional.")
        return email

    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'

        ]
        labels = {
            'username': 'rut',
            'first_name': 'Nombre',
            'last_name':'Apellido',
        }
    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_jefedepto = True
        user.save()
        jefedepto = Jefe_Depto.objects.create(user=user)
        jefedepto.curso.add(*self.cleaned_data.get('curso'))
        jefedepto.interests.add(*self.cleaned_data.get('materias'))
        jefedepto.colegio.add(*self.cleaned_data.get('colegio'))
        jefedepto.email = self.cleaned_data["email"]
        jefedepto.password1 = self.cleaned_data["password1"]
        CorreosJefe.objects.create(jefe=jefedepto, correo=jefedepto.email)

        return user

class StudentSignUpForm(UserCreationForm):
    materias = forms.ModelMultipleChoiceField(

        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )



    curso = forms.ModelMultipleChoiceField(
        queryset=Curso.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    nivel = forms.ModelMultipleChoiceField(
        queryset=Niveles.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)
    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'password1',
            'password2'
        ]
        labels = {
            'username': 'Rut',
            'first_name': 'Primer nombre',
            'last_name': 'Apellido',
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_student = True
        user.save()
        student = Student.objects.create(user=user)
        student.curso.add(*self.cleaned_data.get('curso'))
        student.nivel.add(*self.cleaned_data.get('nivel'))
        student.interests.add(*self.cleaned_data.get('materias'))
        student.colegio.add(*self.cleaned_data.get('colegio'))
        student.password1 = self.cleaned_data["password1"]

        return user

class StudentCursoForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('curso', )
        widgets = {
            'curso': forms.SelectMultiple
        }

class StudentNivelForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('nivel', )
        widgets = {
            'nivel': forms.SelectMultiple
        }


class StudentInterestsForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('interests', )
        label = 'Materias'
        widgets = {
            'interests': forms.CheckboxSelectMultiple
        }

class StudentColegioForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('colegio', )
        widgets = {
            'colegio': forms.CheckboxSelectMultiple
        }


class PlanificacionIndicadorForm(forms.ModelForm):
    class Meta:
        model= PlanificacionIndicador
        exclude=()

PlanificacionIndicadorFormset = inlineformset_factory(
    Planificacion,PlanificacionIndicador, form= PlanificacionIndicadorForm,
    fields=['indicador_evaluacion','dominio_cognitivo'], extra=1, can_delete=True

    )
class PlanificacionForm(forms.ModelForm):

    class Meta:
        model = Planificacion
        exclude=['owner',]
    def __init__(self, *args, **kwargs):
        super(PlanificacionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-6 create-label'
        self.helper.field_class = 'col-md-9'
        self.helper.layout = Layout(
            Div(
                Field('unidad'),
                Field('objetivos_aprendizaje'),
                Formset('indicador'),
                HTML("<br>"),
                Field('curso'),
                Field('asignatura'),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'Guardar')),
                )
            )

class PlanificacionUpdateForm(forms.ModelForm):

    class Meta:
        model = Planificacion
        exclude=['owner','curso','asignatura']
    def __init__(self, *args, **kwargs):
        super(PlanificacionUpdateForm, self).__init__(*args, **kwargs)
        self.fields['unidad'].widget.attrs['readonly'] = True
        self.fields['objetivos_aprendizaje'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-6 create-label'
        self.helper.field_class = 'col-md-9'
        self.helper.layout = Layout(
            Div(
                Field('unidad'),
                Field('objetivos_aprendizaje'),
                Formset('indicador'),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'Guardar')),
                )
            )

class ContactForm(forms.Form):
    Asunto = forms.CharField(required=True)
    Mensaje = forms.CharField(widget=forms.Textarea, required=True)

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('asignatura','category',)

class ColegioForm(forms.ModelForm):
    class Meta:
        model = Colegios
        fields = ('name', 'imagen')

class TextoForm(forms.ModelForm):
    class Meta:
        model = TextoQuestion
        fields = ('titulo','subtitulo','texto','curso',)

class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        context_object_name = 'questions'
        fields = ('number','objetivo','pregunta','indicador','description','text', 'texto' ,'puntaje' ,'image','document','video','url')
        label = 'Pregunta'



    def __init__(self ,quiz ,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['objetivo'].queryset = Planificacion.objects.filter(asignatura__id=quiz.subject.id, curso__id=quiz.curso.id)
        self.fields['indicador'].queryset = PlanificacionIndicador.objects.none()
        self.fields['texto'].queryset = TextoQuestion.objects.filter(owner__id=quiz.owner.id, curso__id=quiz.curso.id)
        self.fields['pregunta'].queryset = Preguntas.objects.filter(asignatura__id=quiz.subject.id, curso__id=quiz.curso.id)

        if 'objetivo' in self.data:
            try:
                objetivo_id = int(self.data.get('objetivo'))
                self.fields['indicador'].queryset = PlanificacionIndicador.objects.filter(objetivo__id=objetivo_id).order_by('indicador_evaluacion')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['indicador'].queryset = self.instance.objetivo.indicador_evaluacion_set.order_by('indicador_evaluacion')

class UpdateQuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        context_object_name = 'questions'
        fields = ('number','objetivo','indicador','pregunta','description','text', 'texto' ,'puntaje' ,'image','document','video','url')
        label = 'Pregunta'



    def __init__(self ,quiz ,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['objetivo'].queryset = Planificacion.objects.filter(asignatura__id=quiz.subject.id, curso__id=quiz.curso.id)
        self.fields['indicador'].queryset= PlanificacionIndicador.objects.filter(objetivo__asignatura__id=quiz.subject.id, objetivo__curso__id=quiz.curso.id)
        self.fields['texto'].queryset = TextoQuestion.objects.filter(owner__id=quiz.owner.id)
        self.fields['pregunta'].queryset = Preguntas.objects.filter(asignatura__id=quiz.subject.id, curso__id=quiz.curso.id)


class TFQuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields =('number','indicador','text','puntaje','image','document')
        label ='Pregunta'

    def __init__(self ,quiz ,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['indicador'].queryset = PlanificacionIndicador.objects.filter(objetivo__asignatura__id=quiz.subject.id, objetivo__curso__id=quiz.curso.id)


class CreateQuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ('correo','name', 'description','max_score','curso','nivel','time')
    
    def clean_correo(self):
        correo = self.cleaned_data['correo'].lower()
        if "@bostoneduca" not  in correo:
            raise ValidationError("Ingrese un correo institucional.")         
        return correo
    


class UpdateQuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ('validar',)
        



class BaseAnswerInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()

        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Marque almenos una respuesta correcta', code='no_correct_answer')


class TakeQuizForm(forms.ModelForm):
    answer = forms.ModelChoiceField(
        queryset=Answer.objects.none(),
        widget=forms.RadioSelect(),
        label='Respuesta',
        required=True,
        empty_label=None)


    class Meta:
        model = StudentAnswer
        fields = ('answer', )
        labels = {
            'answer': 'Respuesta'
        }


    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question')
        super().__init__(*args, **kwargs)
        self.fields['answer'].queryset = question.answers.order_by('question__number')

class PreguntasBanco(forms.ModelForm):
    class Meta:
        model = Preguntas
        fields = ('titulo','curso',)



