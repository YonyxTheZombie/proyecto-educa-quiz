from django.urls import include, path
from django.conf.urls import url
from .views import classroom, students, teachers, fundacion, gestion, jefedepto
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('', classroom.home, name='home'),


    path('gestion/', include(([
        path('', gestion.IndexListView.as_view(), name='indexg'),
    ], 'classroom'), namespace='gestion')),

    path('jefedepto/', include(([
        path('', jefedepto.IndexListView.as_view(), name='index'),
        path('quiz/<int:pk>/', jefedepto.change_quiz, name='quiz_edit'),
        path('quiz/<int:pk>/email/', jefedepto.emailView, name='email'),
        path('success/', jefedepto.successView, name='success'),
        path('quiz/<int:pk>/', jefedepto.quiz_info, name='quiz_info'),
        path('quiz/print', jefedepto.categoria_print, name='categoria_print'),
        path('quiz/print/<int:pk>', jefedepto.categoria_print, name='categoria_print_one'),
        url(r'^reporte_personas_pdf/$',jefedepto.ReportePersonasPDF.as_view(), name="reporte_personas_pdf"),
        path('quiz/pdf/<int:pk>', jefedepto.GeneratePdf.as_view(), name='generatepdf'),
        path('quiz/<int:pk>/results/', jefedepto.QuizResultsView.as_view(), name='quiz_results'),

    ], 'classroom'), namespace='jefedepto')),

    path('fundacion/', include(([
        path('', fundacion.IndexListView.as_view(), name='index'),
        path('colegio/add', fundacion.colegio_add , name='colegio_add'),
        path('user_list', fundacion.user_list, name='user_list'),
        path('colegios', fundacion.colegios, name='colegios'),
        path('quiz/<int:pk>/', fundacion.quiz_info, name='quiz_info'),
        url(r'^reporte_personas_excel/$',fundacion.ReportePersonasExcel.as_view(), name="reporte_personas_excel"),
        url(r'^reporte_todos_excel/$',fundacion.ReporteTodosExcel.as_view(), name="reporte_todos_excel"),




    ], 'classroom'), namespace='fundacion')),


    path('students/', include(([
        path('', students.QuizListView.as_view(), name='quiz_list'),
        path('interests/', students.StudentInterestsView.as_view(), name='student_interests'),
        path('taken/', students.TakenQuizListView.as_view(), name='taken_quiz_list'),
        path('quiz/<int:pk>', students.quiz_info, name='quiz_info'),
        path('quiz/<int:pk>/', students.take_quiz, name='take_quiz'),

    ], 'classroom'), namespace='students')),

    path('teachers/', include(([
        path('pregunta/add/', teachers.crear_banco_preguntas, name='crear_banco'),
        path('banco_preguntas/',teachers.banco_preguntas, name='banco_preguntas'),

        path('texto/add',teachers.TextoCreateView.as_view(), name='texto_add'),
        path('texto_list', teachers.texto_creado, name='texto_list'),
        path('texto/<int:pk>/', teachers.TextoUpdateView.as_view(), name='texto_change'),

        path('categoria_creada', teachers.categoria_creada, name='category_list'),
        path('category/add', teachers.category_add , name='category_add'),
        #tabla de especificaciones
        path('planificacion_list/', teachers.PlanificacionListView.as_view(), name='planificacion_list'),
        path('planificacion/', teachers.HomepageView.as_view(), name='planificacion_all'),
        path('planificacion_curso/', teachers.CursoView.as_view(), name='planificacion_curso'),

        ###################################### Lenguaje ############################################
        path('primero/', teachers.PrimeroBView.as_view(), name='primero'),
        path('segundo/', teachers.SegundoBView.as_view(), name='segundo'),
        path('tercero/', teachers.TerceroBView.as_view(), name='tercero'),
        path('cuarto/', teachers.CuartoBView.as_view(), name='cuarto'),
        path('quinto/', teachers.QuintoBView.as_view(), name='quinto'),
        path('sexto/', teachers.SextoBView.as_view(), name='sexto'),
        path('septimo/', teachers.SeptimoBView.as_view(), name='septimo'),
        path('octavo/', teachers.OctavoBView.as_view(), name='octavo'),
        path('planificacion_primero/', teachers.PrimeroMView.as_view(), name='planificacion_primero'),
        path('planificacion_segundo/', teachers.SegundoMView.as_view(), name='planificacion_segundo'),
        path('planificacion_tercero/', teachers.TerceroMView.as_view(), name='planificacion_tercero'),
        ############################################################################################
        ###################################### Matematicas #########################################
        path('primero/', teachers.PrimeroBView.as_view(), name='Mprimero'),
        path('segundo/', teachers.SegundoBView.as_view(), name='Msegundo'),
        path('tercero/', teachers.TerceroBView.as_view(), name='Mtercero'),
        path('cuarto/', teachers.CuartoBView.as_view(), name='Mcuarto'),
        path('quinto/', teachers.QuintoBView.as_view(), name='Mquinto'),
        path('sexto/', teachers.SextoBView.as_view(), name='Msexto'),
        path('septimo/', teachers.SeptimoBView.as_view(), name='Mseptimo'),
        path('octavo/', teachers.OctavoBView.as_view(), name='Moctavo'),
        path('mate_primero/', teachers.MatePrimeroMView.as_view(), name='mate_primero'),
        path('mate_segundo/', teachers.MateSegundoMView.as_view(), name='mate_segundo'),
        path('mate_tercero/', teachers.MateTerceroMView.as_view(), name='mate_tercero'),
        ############################################################################################
        ###################################### Ciencias #########################################
        path('primero/', teachers.PrimeroBView.as_view(), name='Cprimero'),
        path('segundo/', teachers.SegundoBView.as_view(), name='Csegundo'),
        path('tercero/', teachers.TerceroBView.as_view(), name='Ctercero'),
        path('cuarto/', teachers.CuartoBView.as_view(), name='Ccuarto'),
        path('quinto/', teachers.QuintoBView.as_view(), name='Cquinto'),
        path('sexto/', teachers.SextoBView.as_view(), name='Csexto'),
        path('septimo/', teachers.SeptimoBView.as_view(), name='Cseptimo'),
        path('octavo/', teachers.OctavoBView.as_view(), name='Coctavo'),
        path('ciencias_primero/', teachers.CienciasPrimeroMView.as_view(), name='ciencias_primero'),
        path('ciencias_segundo/', teachers.CienciasSegundoMView.as_view(), name='ciencias_segundo'),
        path('ciencias_tercero/', teachers.CienciasTerceroMView.as_view(), name='ciencias_tercero'),
        ############################################################################################
        ####################################### Ingles #########################################
        path('primero/', teachers.PrimeroBView.as_view(), name='Iprimero'),
        path('segundo/', teachers.SegundoBView.as_view(), name='Isegundo'),
        path('tercero/', teachers.TerceroBView.as_view(), name='Itercero'),
        path('cuarto/', teachers.CuartoBView.as_view(), name='Icuarto'),
        path('quinto/', teachers.QuintoBView.as_view(), name='Iquinto'),
        path('sexto/', teachers.SextoBView.as_view(), name='Isexto'),
        path('septimo/', teachers.SeptimoBView.as_view(), name='Iseptimo'),
        path('octavo/', teachers.OctavoBView.as_view(), name='Ioctavo'),
        path('ingles_primero/', teachers.InglesPrimeroMView.as_view(), name='ingles_primero'),
        path('ingles_segundo/', teachers.InglesSegundoMView.as_view(), name='ingles_segundo'),
        path('ingles_tercero/', teachers.InglesTerceroMView.as_view(), name='ingles_tercero'),
        ############################################################################################
        path('planificacion_ciencias/', teachers.CienciasView.as_view(), name='planificacion_ciencias'),
        path('planificacion_ingles/', teachers.InglesView.as_view(), name='planificacion_ingles'),
        path('planificacion/<int:pk>/results', teachers.PlanificacionDetailView.as_view(), name='planificacion_detail'),
        path('planificacion/add/', teachers.PlanificacionCreateView.as_view(), name='planificacion_add'),
        path('planificacion/<int:pk>/', teachers.PlanificacionUpdateView.as_view(), name='planificacion_change'),
        path('planificacion/delete/<int:pk>/', teachers.PlanificacionDelete.as_view(), name='planificacion_delete'),
        #end

        path('', teachers.QuizListView.as_view(), name='quiz_change_list'),
        path('ajax/load-indi/', teachers.load_indi, name='ajax_load_indi'),
        path('quiz/<int:quiz_pk>/student/', teachers.student, name='student'),
        path('quiz/pdf/<int:pk>', teachers.GeneratePdf.as_view(), name='generatepdf'),
        path('quiz/<int:pk>/mail/', teachers.mail, name='mail'),
        path('quiz/<int:pk>/send/', teachers.send, name='send'),
        path('quiz/add/', teachers.QuizCreateView.as_view(), name='quiz_add'),
        path('quiz/<int:pk>/', teachers.QuizUpdateView.as_view(), name='quiz_change'),
        path('quiz/<int:pk>/delete/', teachers.QuizDeleteView.as_view(), name='quiz_delete'),
        path('quiz/<int:pk>/chart_data/', teachers.chart_data, name='chart_data'),
        path('quiz/<int:pk>/results/', teachers.QuizResultsView.as_view(), name='quiz_results'),
        path('quiz/<int:pk>/question/add/', teachers.question_add, name='question_add'),
        path('quiz/<int:pk>/tfquestion/add/', teachers.tfquestion_add, name='tfquestion_add'),
        path('quiz/<int:quiz_pk>/tfquestion/<int:tfquestion_pk>/', teachers.tfquestion_change, name='tfquestion_change'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/', teachers.question_change, name='question_change'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/delete/', teachers.QuestionDeleteView.as_view(), name='question_delete'),
        path('quiz/<int:quiz_pk>/tfquestion/<int:tfquestion_pk>/delete/', teachers.TFQuestionDeleteView.as_view(), name='tfquestion_delete'),

    ], 'classroom'), namespace='teachers')),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
