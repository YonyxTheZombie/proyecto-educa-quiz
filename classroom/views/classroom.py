from django.shortcuts import redirect, render
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response


from django.views.defaults import page_not_found
 



class SignUpView(TemplateView):
    template_name = 'registration/signup.html'


def home(request):
    if request.user.is_authenticated:
        if request.user.is_teacher:
            return redirect('teachers:quiz_change_list')
        if request.user.is_student:
            return redirect('students:quiz_list')
        if request.user.is_fundacion:
            return redirect('fundacion:index')
        if request.user.is_jefedepto:
            return redirect('jefedepto:index')
        else:
            return redirect('gestion:indexg')
    return render(request, 'classroom/home.html')







  


