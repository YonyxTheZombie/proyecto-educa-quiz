import json
import traceback
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import teacher_required
from ..forms import (BaseAnswerInlineFormSet,QuestionForm,UpdateQuestionForm,TextoForm ,PlanificacionForm,
                        PlanificacionUpdateForm,PlanificacionIndicadorForm,PlanificacionIndicadorFormset,TFQuestionForm,TeacherSignUpForm, CategoryForm, CreateQuizForm, PreguntasBanco)
from ..models import Answer,Planificacion,PlanificacionIndicador,TextoQuestion ,Question, Quiz, User, TakenQuiz, StudentAnswer,  Category, Student, Curso, Colegios, Teacher
from email.mime.image import MIMEImage
from classroom.untils import render_to_pdf #created in step 4
from io import BytesIO
from django.conf import settings
from reportlab.pdfgen import canvas






class TeacherSignUpView(CreateView):
    model = User
    form_class = TeacherSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Profesor'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('teachers:quiz_change_list')

@login_required
@teacher_required
def mail(request, pk):

    quiz = get_object_or_404(Quiz, pk=pk, owner=request.user)


    return render(request,'classroom/teachers/mail.html', {'quiz':quiz})



#Inicio vistas planificacion

@method_decorator([login_required, teacher_required], name='dispatch')
class HomepageView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_all.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class InglesView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_ingles.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CienciasView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_ciencias.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 4).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CursoView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_curso.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2).order_by('id')
        return context


######################################## Inglés #########################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class PrimeroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/primero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 1).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SegundoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/segundo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 5).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class TerceroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/tercero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 6).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CuartoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/cuarto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 7).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class QuintoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/quinto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 8).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SextoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/sexto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 9).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SeptimoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/septimo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 10).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class OctavoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/octavo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 11).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class InglesPrimeroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/primero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 12).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class InglesSegundoMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/segundo_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 13).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class InglesTerceroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ingles/tercero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 5, curso__id= 14).order_by('id')
        return context
###########################################################################################################

######################################## Ciencias #########################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class PrimeroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/primero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 1).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SegundoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/segundo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 5).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class TerceroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/tercero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 6).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CuartoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/cuarto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 7).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class QuintoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/quinto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 8).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SextoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/sexto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 9).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SeptimoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/septimo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 10).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class OctavoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/octavo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 11).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CienciasPrimeroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/primero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 12).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CienciasSegundoMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/segundo_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 13).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CienciasTerceroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Ciencias/tercero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 3, curso__id= 14).order_by('id')
        return context
###########################################################################################################

######################################## Matematicas #########################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class PrimeroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/primero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 1).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SegundoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/segundo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 5).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class TerceroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/tercero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 6).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CuartoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/cuarto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 7).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class QuintoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/quinto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 8).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SextoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/sexto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 9).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SeptimoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/septimo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 10).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class OctavoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/octavo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 11).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class MatePrimeroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/primero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 12).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class MateSegundoMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/segundo_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 13).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class MateTerceroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Matematicas/tercero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 1, curso__id= 14).order_by('id')
        return context
###########################################################################################################


######################################## Lenguaje #########################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class PrimeroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/primero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 1).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SegundoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/segundo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 2).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class TerceroBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/tercero_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 3).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class CuartoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/cuarto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 4).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class QuintoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/quinto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 5).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SextoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/sexto_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 6).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SeptimoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/septimo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 7).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class OctavoBView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/octavo_b.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 8).order_by('id')
        return context


class PrimeroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/primero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 9).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class SegundoMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/segundo_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id=10).order_by('id')
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class TerceroMView(TemplateView):
    template_name = 'classroom/teachers/planificacion/cursos/Lenguaje/tercero_mediol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2, curso__id= 11).order_by('id')
        return context
###########################################################################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class PlanificacionListView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.order_by('id')
        return context



@method_decorator([login_required, teacher_required], name='dispatch')
class LenguajeView(TemplateView):
    template_name = 'classroom/teachers/planificacion/planificacion_lenguaje.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['planificacion'] = Planificacion.objects.filter(asignatura__id= 2).order_by('id')
        return context



@method_decorator([login_required, teacher_required], name='dispatch')
class PlanificacionDetailView(DetailView):
    model = Planificacion
    template_name = 'classroom/teachers/planificacion/planificacion_detail.html'

    def get_context_data(self, **kwargs):
        context = super(PlanificacionDetailView, self).get_context_data(**kwargs)
        return context

@method_decorator([login_required, teacher_required], name='dispatch')
class PlanificacionCreateView(CreateView):
    model= Planificacion
    form_class = PlanificacionForm
    template_name = 'classroom/teachers/planificacion/planificacion_add_form.html'

    def get_context_data(self, **kwargs):
        data = super(PlanificacionCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['indicador'] = PlanificacionIndicadorFormset(self.request.POST)
        else:
            data['indicador'] = PlanificacionIndicadorFormset()
        return data

    def form_valid(self, form):
        context= self.get_context_data()
        indicador = context['indicador']
        with transaction.atomic():
            form.instance.owner = self.request.user
            self.indicador_evaluacion = form.save()
            if indicador.is_valid():
                indicador.instance = self.indicador_evaluacion
                indicador.save()
            return super(PlanificacionCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('teachers:planificacion_detail', kwargs={'pk': self.indicador_evaluacion.pk})

@method_decorator([login_required, teacher_required], name='dispatch')
class PlanificacionUpdateView(UpdateView):
    model= Planificacion
    form_class = PlanificacionUpdateForm
    template_name = 'classroom/teachers/planificacion/planificacion_add_form.html'

    def get_context_data(self, **kwargs):
        data = super(PlanificacionUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['indicador'] = PlanificacionIndicadorFormset(self.request.POST, instance=self.object)
        else:
            data['indicador'] = PlanificacionIndicadorFormset(instance=self.object)
        return data

    def form_valid(self, form):
        context= self.get_context_data()
        indicador = context['indicador']
        with transaction.atomic():
            form.instance.owner = self.request.user
            self.indicador_evaluacion = form.save()
            if indicador.is_valid():
                indicador.instance = self.indicador_evaluacion
                indicador.save()
            return super(PlanificacionUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('teachers:planificacion_detail', kwargs={'pk': self.indicador_evaluacion.pk})

@method_decorator([login_required, teacher_required], name='dispatch')
class PlanificacionDelete(DeleteView):
    model = Planificacion
    template_name = 'classroom/teachers/planificacion/planificacion_delete.html'
    success_url = reverse_lazy('teachers:quiz_change_list')

########################fin vista planificación##########################################
#########################################################################################
@method_decorator([login_required, teacher_required], name='dispatch')
class QuizCreateView(CreateView):
    model = Quiz
    context_object_name = 'quizzes'
    form_class = CreateQuizForm 
    template_name = 'classroom/teachers/quiz_add_form.html'


    def form_valid(self, form):
        quiz = form.save(commit=False)
        quiz.porcen_pro = 60
        quiz.owner = self.request.user

        if quiz.owner.email in quiz.correo:
            messages.error(self.request, "Ingrese correo de jefe de departamento, no el suyo.")
            return redirect('teachers:quiz_add')


        quiz.colegio= self.request.user.teacher.colegio.all()[0]
        quiz.subject= self.request.user.teacher.interests.all()[0]
        quiz.save()
        messages.success(self.request, 'El examen fue creado ahora agreguemos algunas preguntas')
        return redirect('teachers:quiz_change', quiz.pk)

@login_required
@teacher_required
def send(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk, owner=request.user)

    subject = "Examen Creado"
    html_body = render_to_string("classroom/teachers/mail.html", {'quiz':quiz})
    msg = EmailMultiAlternatives(subject=subject, from_email="notificacionesbostoneduca@gmail.com",
                                to=[quiz.correo], body=html_body)
    msg.attach_alternative(html_body, "text/html")
    msg.send()
    messages.success(request, 'Mensaje Enviado')

    return redirect('teachers:quiz_change', quiz.pk)

@method_decorator([login_required, teacher_required], name='dispatch')
class QuizUpdateView(UpdateView):
    model = Quiz
    fields = ('name','description' , 'porcen_pro','max_score' , 'curso','nivel','lock', 'time')
    context_object_name = 'quiz'
    template_name = 'classroom/teachers/quiz_change_form.html'

    def get_context_data(self, **kwargs):


        kwargs['questions'] = self.get_object().questions.annotate(answers_count=Count('answers', distinct=True)).order_by('number')
        return super().get_context_data(**kwargs)



    def get_queryset(self):
        '''
        Este método es una gestión de permisos implícita a nivel de objeto.
        Esta vista solo coincidirá con los ID de las pruebas existentes que pertenecen
        al usuario registrado.

        '''
        return self.request.user.quizzes.all()

    def get_success_url(self):
        return reverse('teachers:quiz_change', kwargs={'pk': self.object.pk})


@method_decorator([login_required, teacher_required], name='dispatch')
class QuizDeleteView(DeleteView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'classroom/teachers/quiz_delete_confirm.html'
    success_url = reverse_lazy('teachers:quiz_change_list')

    def delete(self, request, *args, **kwargs):
        quiz = self.get_object()
        messages.success(request, 'El examen  %s ah sido borrado!' % quiz.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.quizzes.all()


@method_decorator([login_required, teacher_required], name='dispatch')
class QuizListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'classroom/teachers/quiz_change_list.html'

    def get_queryset(self):
        queryset = self.request.user.quizzes \
            .select_related('subject', 'colegio') \
            .annotate(questions_count=Count('questions', distinct=True)) \
            .annotate(taken_count=Count('taken_quizzes', distinct=True))
        return queryset



@login_required
@teacher_required
def chart_data(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    taken_quizzesrepro = quiz.taken_quizzes.select_related('student__user').filter(score__lt= 4.0).order_by('student')
    taken_quizzesapro = quiz.taken_quizzes.select_related('student__user').filter(score__gt= 4.0).order_by('student')
    taken_quizzes = quiz.taken_quizzes.select_related('student__user').order_by('-score')
    minimo = quiz.taken_quizzes.aggregate(min=Min('score'))
    maximo = quiz.taken_quizzes.aggregate(max=Max('score'))
    questioncount = Question.objects.filter(quiz=quiz.id).values('objetivo').annotate(count=Count('objetivo')).order_by('objetivo')
    question = Question.objects.filter(quiz=quiz.id).order_by('objetivo')
    answer=Answer.objects.filter(question__quiz=quiz)
    student = StudentAnswer.objects.filter(answer__question__quiz=quiz, answer__is_correct=True).order_by('student')
    studentcorrecta = StudentAnswer.objects.filter(answer__question__quiz=quiz, answer__is_correct=True).values('answer').annotate(count=Count('answer')).order_by('answer')
    studentincorrecta = StudentAnswer.objects.filter(answer__question__quiz=quiz, answer__is_correct=False).values('answer').annotate(count=Count('answer')).order_by('answer')
    puntaje_maximo= quiz.max_score
    porcentaje_aprobacion= quiz.porcen_pro
    puntaje_aprobacion = (puntaje_maximo*porcentaje_aprobacion)/100
    studentcount = taken_quizzes.count()
    studentcountr = taken_quizzesrepro.count()
    studentcounta = taken_quizzesapro.count()
    porcentaje_reprobados = round((100*studentcountr)/studentcount , 2)
    porcentaje_aprobados = round((100*studentcounta)/studentcount , 2)

    return render(request, 'classroom/teachers/chart_data.html', {
        'quiz':quiz,
        'taken_quizzes':taken_quizzes,
        'minimo':minimo,
        'maximo':maximo,
        'question':question,
        'answer':answer,
        'student':student,
        'studentcorrecta':studentcorrecta,
        'studentincorrecta':studentincorrecta,
        'questioncount':questioncount,
        'puntaje_aprobacion':puntaje_aprobacion,
        'studentcount':studentcount,
        'taken_quizzesrepro':taken_quizzesrepro,
        'taken_quizzesapro ':taken_quizzesapro ,
        'studentcounta':studentcounta,
        'studentcountr':studentcountr,
        'porcentaje_reprobados':porcentaje_reprobados,
        'porcentaje_aprobados':porcentaje_aprobados,

    })


@login_required
@teacher_required
def student(request, quiz_pk):
    quiz = get_object_or_404(Quiz, pk=quiz_pk)
    question =  Question.objects.filter(quiz=quiz.id).order_by('number')
    answer=Answer.objects.filter(question__quiz=quiz)
    student = StudentAnswer.objects.filter(answer__question__quiz=quiz)
    return render(request, "classroom/teachers/student.html",{
        'quiz':quiz,
        'question':question,
        'answer':answer,
        'student':student,

    })

class Round(Func):

    function = 'ROUND'
    template='%(function)s(%(expressions)s, 0)'


@method_decorator([login_required, teacher_required], name='dispatch')
class QuizResultsView(DetailView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'classroom/teachers/quiz_results.html'

    def get_context_data(self, **kwargs):
        quiz = self.get_object()
        taken_quizzes = quiz.taken_quizzes.select_related('student__user').order_by('-score')
        total_taken_quizzes = taken_quizzes.count()
        quiz_score = quiz.taken_quizzes.aggregate(average_score=Round(Avg('score')))

        extra_context = {
            'taken_quizzes': taken_quizzes,
            'total_taken_quizzes': total_taken_quizzes,
            'quiz_score': quiz_score
        }
        kwargs.update(extra_context)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return self.request.user.quizzes.all()


@method_decorator([login_required, teacher_required], name='dispatch')
class TextoCreateView(CreateView):
    model = TextoQuestion
    context_object_name = 'texto'
    fields = ('titulo','subtitulo','texto','curso')
    template_name = 'classroom/teachers/texto_add_form.html'


    def form_valid(self, form):
        texto = form.save(commit=False)
        texto.owner = self.request.user
        texto.save()
        messages.success(self.request, 'Se ah creado un texto')
        return redirect('teachers:texto_list')



@login_required
@teacher_required
def texto_creado( request):
    texto = TextoQuestion.objects.all().filter(owner__id=request.user.id).order_by('id')
    context = {'texto':texto}
    return render(request, 'classroom/teachers/texto_list.html', context )

@method_decorator([login_required, teacher_required], name='dispatch')
class TextoUpdateView(UpdateView):
    model = TextoQuestion
    context_object_name = 'texto'
    fields = ('titulo','subtitulo','texto','curso')
    template_name = 'classroom/teachers/texto_change_form.html'


    def get_queryset(self):
        '''
        Este método es una gestión de permisos implícita a nivel de objeto.
        Esta vista solo coincidirá con los ID de las pruebas existentes que pertenecen
        al usuario registrado.

        '''
        return self.request.user.texto.all()

    def get_success_url(self):
        return reverse('teachers:texto_list')

@login_required
@teacher_required
def category_add(request):




    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            messages.success(request, 'Contenido creado')
            return redirect('teachers:category_list')
    else:
        form = CategoryForm()


    return render(request, 'classroom/teachers/category_add_form.html', {'form': form})



@login_required
@teacher_required
def categoria_creada(request):
    categoria = Category.objects.all().order_by('asignatura')
    context = {'categoria':categoria}
    return render(request, 'classroom/teachers/category_list.html', context )

@login_required
@teacher_required
def load_indi(request):
    objetivo_id = request.GET.get('objetivo')
    indicador = PlanificacionIndicador.objects.filter(objetivo__id=objetivo_id).order_by('indicador_evaluacion')
    return render(request, 'classroom/teachers/dropdown.html', {'indicador':indicador})


@login_required
@teacher_required
def question_add(request, pk):

    #Al filtrar el cuestionario por el argumento de la palabra clave url `pk` y
    #por el propietario, que es el usuario registrado, estamos protegiendo
    #esta vista a nivel de objeto. Es decir, sólo el propietario de
    #El cuestionario podrá agregarle preguntas.
    quiz = get_object_or_404(Quiz, pk=pk, owner=request.user)

    if request.method == 'POST':
        form = QuestionForm(quiz, request.POST, request.FILES)
        if form.is_valid():
            question = form.save(commit=False)
            question.quiz = quiz
            question.save()
            messages.success(request, 'Ahora puede agregar respuestas / opciones a la pregunta.')
            return redirect('teachers:question_change', quiz.pk, question.pk)
    else:
        form = QuestionForm(quiz=quiz)
    return render(request, 'classroom/teachers/question_add_form.html', {'quiz': quiz,'form': form})

@login_required
@teacher_required
def tfquestion_add(request, pk):

    #Al filtrar el cuestionario por el argumento de la palabra clave url `pk` y
    #por el propietario, que es el usuario registrado, estamos protegiendo
    #esta vista a nivel de objeto. Es decir, sólo el propietario de
    #El cuestionario podrá agregarle preguntas.
    quiz = get_object_or_404(Quiz, pk=pk, owner=request.user)



    if request.method == 'POST':
        form = TFQuestionForm(quiz, request.POST,  request.FILES)

        if form.is_valid():
            tfquestion = form.save(commit=False)
            tfquestion.quiz = quiz
            tfquestion.save()
            messages.success(request, 'Ahora puede agregar respuestas / opciones a la pregunta.')
            return redirect('teachers:tfquestion_change', quiz.pk, tfquestion.pk)
    else:
        form = TFQuestionForm(quiz=quiz)


    return render(request, 'classroom/teachers/tfquestion_add_form.html', {'quiz': quiz, 'form': form})



@login_required
@teacher_required
def tfquestion_change(request, quiz_pk, tfquestion_pk):

# Simlar a la vista 'question_add', esta vista también administra
# los permisos a nivel de objeto. Al consultar tanto 'quiz' como
# 'question' nos aseguramos de que solo el propietario del cuestionario pueda
# Cambiar sus detalles y también solo las preguntas que pertenecen a esta.
# prueba específica se puedan cambiar a través de esta URL (en los casos en que el
# usuario podría haber falsificado / jugado con los parámetros url.
    quiz = get_object_or_404(Quiz, pk=quiz_pk, owner=request.user)
    tfquestion = get_object_or_404(Question, pk=tfquestion_pk, quiz=quiz)

    AnswerFormSet = inlineformset_factory(
        Question,  # modelo padre
        Answer,  # modelo base
        formset=BaseAnswerInlineFormSet,
        fields=('text','answer_order' ,'is_correct'),
        min_num=2,
        validate_min=True,
        max_num=2,
        validate_max=True
    )



    if request.method == 'POST':
        form = TFQuestionForm(quiz,request.POST, request.FILES ,instance=tfquestion )
        formset = AnswerFormSet(request.POST, request.FILES, instance=tfquestion)
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                form.save()
                formset.save()
            messages.success(request, 'Pregunta y respuestas guardadas exitosamente!')
            return redirect('teachers:quiz_change', quiz.pk)
    else:
        form = TFQuestionForm(instance=tfquestion, quiz=quiz)
        formset = AnswerFormSet(instance=tfquestion)

    return render(request, 'classroom/teachers/tfquestion_change_form.html', {
        'quiz': quiz,
        'tfquestion': tfquestion,
        'form': form,
        'formset': formset
    })

@login_required
@teacher_required
def question_change(request, quiz_pk, question_pk):

# Simlar a la vista 'question_add', esta vista también administra
# los permisos a nivel de objeto. Al consultar tanto 'quiz' como
# 'question' nos aseguramos de que solo el propietario del cuestionario pueda
# Cambiar sus detalles y también solo las preguntas que pertenecen a esta.
# prueba específica se puedan cambiar a través de esta URL (en los casos en que el
# usuario podría haber falsificado / jugado con los parámetros url.
    quiz = get_object_or_404(Quiz, pk=quiz_pk, owner=request.user)
    question = get_object_or_404(Question, pk=question_pk, quiz=quiz)

    AnswerFormSet = inlineformset_factory(
        Question,  # modelo padre
        Answer,  # modelo base
        formset=BaseAnswerInlineFormSet,
        fields=('text' ,'is_correct'),
        min_num=2,
        validate_min=True,
        max_num=10,
        validate_max=True,
    )



    if request.method == 'POST':
        form = UpdateQuestionForm(quiz ,request.POST, request.FILES ,instance=question)
        formset = AnswerFormSet(request.POST, request.FILES, instance=question)
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                form.save()
                formset.save()
            messages.success(request, 'Pregunta y respuestas guardadas exitosamente!')
            return redirect('teachers:quiz_change', quiz.pk)
    else:
        form = UpdateQuestionForm(instance=question, quiz=quiz)
        formset = AnswerFormSet(instance=question)

    return render(request, 'classroom/teachers/question_change_form.html', {
        'quiz': quiz,
        'question': question,
        'form': form,
        'formset': formset
    })






@method_decorator([login_required, teacher_required], name='dispatch')
class QuestionDeleteView(DeleteView):
    model = Question
    context_object_name = 'question'
    template_name = 'classroom/teachers/question_delete_confirm.html'
    pk_url_kwarg = 'question_pk'

    def get_context_data(self, **kwargs):
        question = self.get_object()
        kwargs['quiz'] = question.quiz
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        question = self.get_object()
        messages.success(request, 'La pregunta %s ah sido borrada!' % question.text  )
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return Question.objects.filter(quiz__owner=self.request.user)

    def get_success_url(self):
        question = self.get_object()
        return reverse('teachers:quiz_change', kwargs={'pk': question.quiz_id})



@method_decorator([login_required, teacher_required], name='dispatch')
class TFQuestionDeleteView(DeleteView):
    model = Question
    context_object_name = 'tfquestion'
    template_name = 'classroom/teachers/tfquestion_delete_confirm.html'
    pk_url_kwarg = 'tfquestion_pk'

    def get_context_data(self, **kwargs):
        tfquestion = self.get_object()
        kwargs['quiz'] = tfquestion.quiz
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        tfquestion = self.get_object()
        messages.success(request, 'La pregunta %s ah sido borrada!' % tfquestion.text  )
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return Question.objects.filter(quiz__owner=self.request.user)

    def get_success_url(self):
        tfquestion = self.get_object()
        return reverse('teachers:quiz_change', kwargs={'pk': tfquestion.quiz_id})


@method_decorator([login_required, teacher_required], name='dispatch')
class GeneratePdf(View):



    def get(self, request, pk ,*args, **kwargs):
        template = get_template('classroom/teachers/invoice.html')
        archivo_imagen = settings.MEDIA_ROOT+'/img/logo.jpg'
        imagenequiz = settings.MEDIA_ROOT+'/colegio_images/'
        quiz =  Quiz.objects.filter(id=pk)
        question =  Question.objects.filter(quiz__id__in=quiz.values_list('id'))
        answer=Answer.objects.filter(question__id__in=question.values_list("id"))
        context = {
            'quizimg':imagenequiz,
            'img': archivo_imagen,
            'quiz':quiz,
            'question':question,
            'answer':answer,
        }
        html = template.render(context)
        pdf = render_to_pdf('classroom/teachers/invoice.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename= "Invoice_%s.pdf" %("12341231")
            content = "inline; filename='%s'"%(filename)
            download = request.GET.get("download")
            if download:
                content="attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

def crear_banco_preguntas(request):
    
    if request.method == 'POST':
        form = PreguntasBanco(request.POST)
        if form.is_valid():
            pregunta = form.save(commit=False)
            pregunta.asignatura = request.user.teacher.interests.all()[0]
            pregunta.save()
            messages.success(request, 'Pregunta creada')
            return redirect('teachers:banco_preguntas')
    else:
        form = PreguntasBanco()

    return render(request, 'classroom/teachers/creacion_pregunta_banco.html', {'form':form})


def banco_preguntas(request):
    
    return render(request, 'classroom/teachers/banco_preguntas.html')

