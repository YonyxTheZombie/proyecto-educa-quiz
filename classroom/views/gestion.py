import json
import traceback
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import gestion_required
from ..forms import BaseAnswerInlineFormSet,QuestionForm, GestionSignUpForm,  CategoryForm, ColegioForm
from ..models import Answer, Question, Quiz, User, TakenQuiz, StudentAnswer, Category,  Colegios





class GestionSignUpView(CreateView):
    model = User
    form_class = GestionSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'gestion'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('gestion:indexg')


@method_decorator([login_required, gestion_required], name='dispatch')
class IndexListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'classroom/gestion/indexg.html'

    def get_queryset(self):
        gestion = self.request.user.gestion
        gestion_interests = gestion.interests.values_list('pk', flat=True)
        gestion_curso = gestion.curso.values_list('pk', flat=True)
        gestion_colegio = gestion.colegio.values_list('pk', flat=True)
        taken_quizzes = gestion.quizzes.values_list('pk', flat=True)
        queryset = Quiz.objects.filter(curso__in=gestion_curso, subject__in=gestion_interests, colegio__in=gestion_colegio) \
            .exclude(pk__in=taken_quizzes) 
        return queryset