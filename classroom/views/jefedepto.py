import json
import traceback
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import jefedepto_required
from ..forms import BaseAnswerInlineFormSet,QuestionForm, ContactForm,JefeDSignUpForm,  CategoryForm, UpdateQuizForm, ColegioForm, TakeQuizForm
from ..models import Answer, Question, Quiz, User, TakenQuiz, StudentAnswer, Category,  Colegios
from classroom.untils import render_to_pdf #created in step 4
from io import BytesIO
from django.conf import settings
from reportlab.pdfgen import canvas




class JefeDSignUpView(CreateView):
    model = User
    form_class = JefeDSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'jefedepto'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('jefedepto:index')


@method_decorator([login_required, jefedepto_required], name='dispatch')
class IndexListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'classroom/jefedepto/index.html'

    def get_queryset(self):
        jefedepto = self.request.user.jefe_depto
        jefedepto_interests = jefedepto.interests.values_list('pk', flat=True)
        jefedepto_curso = jefedepto.curso.values_list('pk', flat=True)
        jefedepto_colegio = jefedepto.colegio.values_list('pk', flat=True)
        queryset = Quiz.objects.filter(curso__in=jefedepto_curso, subject__in=jefedepto_interests, colegio__in=jefedepto_colegio)
        return queryset


@login_required
@jefedepto_required
def change_quiz(request, pk):

    quiz= get_object_or_404(Quiz, pk=pk)
    question = Question.objects.filter(quiz=quiz)
    questionsum = Question.objects.filter(quiz=quiz).values('puntaje').annotate(sumar=Sum('puntaje')).order_by('puntaje')
    answer = Answer.objects.filter(question__id__in=question.values_list("id"))
    if request.method == 'POST':
        form = UpdateQuizForm(request.POST,  request.FILES, instance=quiz)
        if form.is_valid():
            with transaction.atomic():
                
                form.save()
            messages.success(request, 'Prueba Editada')
            return redirect('jefedepto:index')
    else:
        form = UpdateQuizForm(instance=quiz)

    return render(request, 'classroom/jefedepto/quiz_edit.html',{'quiz':quiz,'questionsum':questionsum,'question':question,'answer':answer,'form':form})


@login_required
@jefedepto_required
def quiz_info(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)

    question =  Question.objects.filter(quiz=quiz.id)
    answer=Answer.objects.filter(question__id__in=question.values_list("id"))
    return render(request, 'classroom/jefedepto/quiz_info.html', {
        'quiz': quiz,
        'question':question,
        'answer':answer
    })





class QuestionView(DetailView):

    model = Question
    context_object_name = 'question'
    template_name = 'classroom/jefedepto/quiz_info_question.html'
    pk_url_kwarg = 'question_pk'

    def get_context_data(self, **kwargs):
        question = self.get_object()
        kwargs['quiz'] = question.quiz
        return super().get_context_data(**kwargs)


    def get_queryset(self):
        return Question.objects.filter(quiz__owner=self.request.user)


def categoria_print(self, pk=None):
   import io
   from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
   from reportlab.lib.styles import getSampleStyleSheet
   from reportlab.lib import colors
   from reportlab.lib.pagesizes import letter
   from reportlab.platypus import Table

   response = HttpResponse(content_type='application/pdf')
   buff = io.BytesIO()
   doc = SimpleDocTemplate(buff,
               pagesize=letter,
               rightMargin=40,
               leftMargin=40,
               topMargin=60,
               bottomMargin=18,
               )
   pruebas = []
   styles = getSampleStyleSheet()
   header = Paragraph("Listado de pruebas", styles['Heading1'])
   pruebas.append(header)
   headings = ('Id', 'Nombre', 'Profesor', 'Colegio' ,'Creación')
   if not pk:
     todaspruebas = [(quiz.id, quiz.name, quiz.owner, quiz.colegio ,quiz.created)
               for quiz in Quiz.objects.all().order_by('pk')]
   else:
     todaspruebas = [(quiz.id, quiz.name, quiz.owner, quiz.colegio ,quiz.created)
               for quiz in Quiz.objects.filter(id=pk)]
   t = Table([headings] + todaspruebas)
   t.setStyle(TableStyle(
     [
       ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
       ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
       ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
     ]
   ))

   pruebas.append(t)
   doc.build(pruebas)
   response.write(buff.getvalue())
   buff.close()
   return response






class ReportePersonasPDF(View):

    def cabecera(self,pdf):
        #Utilizamos el archivo logo_django.png que está guardado en la carpeta media/imagenes
        archivo_imagen = settings.MEDIA_ROOT+'/img/logo.jpg'
        #Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
        pdf.drawImage(archivo_imagen, 40, 750, 120, 90,preserveAspectRatio=True)
        #Establecemos el tamaño de letra en 16 y el tipo de letra Helvetica
        pdf.setFont("Helvetica", 16)
        #Dibujamos una cadena en la ubicación X,Y especificadadef GeneratePdf(view):
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"REPORTE DE USUARIOS")

    def get(self, request, *args, **kwargs):
        #Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        #La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        #Canvas nos permite hacer el reporte con coordenadas X y Y
        pdf = canvas.Canvas(buffer)
        #Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
        self.cabecera(pdf)
        #Con show page hacemos un corte de página para pasar a la siguiente
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response






@method_decorator([login_required, jefedepto_required], name='dispatch')
class GeneratePdf(View):



    def get(self, request, pk ,*args, **kwargs):
        template = get_template('classroom/jefedepto/invoice.html')
        archivo_imagen = settings.MEDIA_ROOT+'/img/logo.jpg'
        css = ['static/css/bootstrap.min.css']
        quiz =  Quiz.objects.filter(id=pk)
        question =  Question.objects.filter(quiz__id__in=quiz.values_list('id'))
        answer=Answer.objects.filter(question__id__in=question.values_list("id"))
        context = {
            'img': archivo_imagen,
            'invoice_id':123,
            'css':css,
            'quiz':quiz,
            'question':question,
            'answer':answer,
            'customer_name': 'John Cooper',
            'amount': 39.99,
            'today': 'Today',
        }
        html = template.render(context)
        pdf = render_to_pdf('classroom/jefedepto/invoice.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename= "Invoice_%s.pdf" %("12341231")
            content = "inline; filename='%s'"%(filename)
            download = request.GET.get("download")
            if download:
                content="attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

class Round(Func):

    function = 'ROUND'
    template='%(function)s(%(expressions)s, 0)'


@method_decorator([login_required, jefedepto_required], name='dispatch')
class QuizResultsView(DetailView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'classroom/jefedepto/quiz_results.html'

    def get_context_data(self, **kwargs):
        quiz = self.get_object()
        taken_quizzes = quiz.taken_quizzes.select_related('student__user').order_by('-score')
        total_taken_quizzes = taken_quizzes.count()
        quiz_score = quiz.taken_quizzes.aggregate(average_score=Round(Avg('score')))

        extra_context = {
            'taken_quizzes': taken_quizzes,
            'total_taken_quizzes': total_taken_quizzes,
            'quiz_score': quiz_score
        }
        kwargs.update(extra_context)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        jefedepto = self.request.user.jefe_depto
        jefedepto_interests = jefedepto.interests.values_list('pk', flat=True)
        jefedepto_curso = jefedepto.curso.values_list('pk', flat=True)
        jefedepto_colegio = jefedepto.colegio.values_list('pk', flat=True)
        taken_quizzes = jefedepto.quizzes.values_list('pk', flat=True)
        queryset = Quiz.objects.filter(curso__in=jefedepto_curso, subject__in=jefedepto_interests, colegio__in=jefedepto_colegio) \
            .exclude(pk__in=taken_quizzes)
        return queryset


def emailView(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)

    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['Asunto']
            body = form.cleaned_data['Mensaje']
            msg = EmailMultiAlternatives(subject=subject, from_email="notificacionesbostoneduca@gmail.com",
                                to=[quiz.owner.email], body=body)
            msg.send()

            return redirect('jefedepto:index')
    return render(request, "classroom/jefedepto/email.html", {'quiz':quiz,'form': form})

def successView(request):
    return HttpResponse('Success! Thank you for your message.')