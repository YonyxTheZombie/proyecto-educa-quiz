import xlwt
import json
import traceback
from openpyxl import Workbook
from datetime import timedelta
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from classroom import resources
from ..decorators import fundacion_required
from ..forms import BaseAnswerInlineFormSet,QuestionForm, TakeQuizForm,FundacionSignUpForm, CategoryForm, ColegioForm
from ..models import Answer, Question, Quiz, User, TakenQuiz, StudentAnswer,  Category, Colegios, Teacher
from tablib import Dataset

class ReporteTodosExcel(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        personas = User.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'TODOS LOS USUARIOS'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:E1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['A3'] = 'RUT'
        ws['B3'] = 'NOMBRE'
        ws['C3'] = 'APELLIDO'    
        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for persona in personas:
    
            ws.cell(row=cont,column=1).value = persona.username
            ws.cell(row=cont,column=2).value = persona.first_name
            ws.cell(row=cont,column=3).value = persona.last_name
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReporteUsuarios.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReportePersonasExcel(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        personas = Teacher.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'PROFESORES'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:D1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['A3'] = 'RUT'
        ws['B3'] = 'NOMBRE'
        ws['C3'] = 'APELLIDO'
        ws['D3'] = 'ASIGNATURA'
        ws['E3'] = 'CORREO'     
        cont=5
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for persona in personas:
            
            ws.cell(row=cont,column=1).value = persona.user.username
            ws.cell(row=cont,column=2).value = persona.user.first_name
            ws.cell(row=cont,column=3).value = persona.user.last_name
            for interests in persona.interests.all():
                ws.cell(row=cont,column=4).value = interests.name
            ws.cell(row=cont,column=5).value = persona.user.email
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReporteProfesores.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


class FundacionSignUpView(CreateView):
    model = User
    form_class = FundacionSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Fundacion'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('fundacion:index')

@login_required
@fundacion_required
def index(request):
    
    return render(request, 'classroom/fundacion/index.html')

@method_decorator([login_required, fundacion_required], name='dispatch')
class IndexListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'classroom/fundacion/index.html'

    def get_queryset(self):
        fundacion = self.request.user.fundacion
        fundacion_interests = fundacion.interests.values_list('pk', flat=True)
        fundacion_curso = fundacion.curso.values_list('pk', flat=True)
        fundacion_colegio = fundacion.colegio.values_list('pk', flat=True)
        taken_quizzes = fundacion.quizzes.values_list('pk', flat=True)
        queryset = Quiz.objects.filter(curso__in=fundacion_curso, subject__in=fundacion_interests, colegio__in=fundacion_colegio) \
            .exclude(pk__in=taken_quizzes)
        return queryset

@login_required
@fundacion_required
def colegio_add(request):
        


    if request.method == 'POST':
        form = ColegioForm(request.POST, request.FILES)
        if form.is_valid():
            colegio = form.save(commit=False)
            colegio.save()
            messages.success(request, 'Colegios agregados')
            return redirect('fundacion:index')
    else:
        form = ColegioForm()


    return render(request, 'classroom/fundacion/colegios_form.html', {'form': form})


@login_required
@fundacion_required
def user_list(request):
    user= User.objects.order_by('-is_teacher','-is_jefedepto','-is_student')
    contexto = {'user':user}

    return render(request, 'classroom/fundacion/user_list.html', contexto)

@login_required
@fundacion_required
def colegios(request):
    colegios=Colegios.objects.all()
    contexto = {'colegios':colegios}
    return render(request, 'classroom/fundacion/colegios.html', contexto)


@login_required
@fundacion_required
def quiz_info(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)

    question =  Question.objects.filter(quiz=quiz.id)
    answer=Answer.objects.filter(question__id__in=question.values_list("id"))
    
    
    return render(request, 'classroom/fundacion/quiz_info.html', {
        'quiz': quiz,
        'question':question,
        'answer':answer,
    
    })

