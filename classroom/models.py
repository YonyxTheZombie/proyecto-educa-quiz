import sys
import math
import random
from io import BytesIO
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe
from model_utils.managers import InheritanceManager
from django.utils.timezone import utc


ANSWER_ORDER_OPTIONS = (
    ('text', ('Text')),
    ('random', ('Random')),
    ('none', ('None'))
)
class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_gestion = models.BooleanField(default=False)
    is_jefedepto = models.BooleanField(default=False)
    is_fundacion = models.BooleanField(default=False)

def colegios_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)


class Niveles(models.Model):
    nivel = models.CharField(max_length=10)

    def __str__(self):
        return self.nivel

class Colegios(models.Model):
    name = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='colegios_images',default=False, blank=True,verbose_name=('Imagen'), unique=True)

    def __str__(self):
        return self.name

    def get_html_badge(self):
        name = escape(self.name)
        html = '<span class="badge ">%s</span>' % ( name)
        return mark_safe(html)


class CategoryManager(models.Manager):

    def new_category(self, category):
        new_category = self.create(category=re.sub('\s+', '-', category)
                                   .lower())

        new_category.save()
        return new_category





def quiz_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)


class Dominio(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Subject(models.Model):
    name = models.CharField(max_length=30)
    color = models.CharField(max_length=7, default='#007bff')

    def __str__(self):
        return self.name

    def get_html_badge(self):
        name = escape(self.name)
        color = escape(self.color)
        html = '<span class="badge badge-primary" style="background-color: %s">%s</span>' % (color, name)
        return mark_safe(html)
class Category(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="questions")
    asignatura = models.ForeignKey(Subject, on_delete=models.CASCADE)
    category = models.CharField(
        verbose_name=_("Contenido"),
        max_length=250, blank=True,
        unique=True, null=True)

    objects = CategoryManager()

    class Meta:
        verbose_name = _("Catgoría")
        verbose_name_plural = _("Categorías")

    def __str__(self):
        return self.category

class Curso(models.Model):
    nombre = models.CharField(max_length=30)
    color = models.CharField(max_length=7, default='#007bff')

    def __str__(self):
        return self.nombre

    def get_html_badge(self):
        nombre = escape(self.nombre)
        color = escape(self.color)
        html = '<span class="badge badge-primary" style="background-color: %s">%s</span>' % (color, nombre)
        return mark_safe(html)

class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    colegio = models.ManyToManyField(Colegios, related_name='colegio_teachers', blank=True)
    interests = models.ManyToManyField(Subject, related_name='interested_teacher', verbose_name=('Asignaturas'), default=True)

    def __str__(self):
        return self.user.first_name

class Quiz(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='quizzes')
    name = models.CharField(max_length=255, verbose_name=('Titulo'))
    description = models.TextField(max_length=2000, verbose_name=('Instrucciones'))
    colegio = models.ForeignKey(Colegios, on_delete=models.CASCADE, related_name='quizzes')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='quizzes', verbose_name= ("Asignatura"))
    curso = models.ForeignKey(Curso, on_delete= models.CASCADE, related_name='quizzes')
    nivel = models.ForeignKey(Niveles, on_delete=models.CASCADE,verbose_name="Letra" ,related_name='quizzes', help_text="Letra del curso" )
    lock = models.BooleanField(null= False, default = True, verbose_name=('Bloquear'), help_text="Solo estara disponible para ti el alumno no vera el examen a no ser que desactives esta opción.")
    validar = models.BooleanField(null= False, default = False, verbose_name=('validar'), help_text="Validador de prueba.")
    time = models.IntegerField(verbose_name=('Duración'),help_text="Se deben ingresar en minutos es decir 1 es un minuto, 2 dos minutos y asi.")
    max_score = models.IntegerField(verbose_name=('Puntaje máximo'), help_text="El puntaje máximo que tendrá la prueba.")
    porcen_pro = models.IntegerField(verbose_name=("Porcentaje de aprobación"))
    correo = models.EmailField(verbose_name=('Correo jefe de departamento'), help_text="Debes ingresar el correo de tu jefe de departamento.")
    created = models.DateField(auto_now_add=True)
    random_order = models.BooleanField(
        blank=False, default=False,
        verbose_name=_("Orden Random"),
        help_text=_("Muestra el orden de las respuestas en orden aleatorio"))



    def __str__(self):
        return self.name



class QuizFor(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='quizzesf')
    name = models.CharField(max_length=255, verbose_name=('Titulo'))
    description = models.TextField(max_length=500, verbose_name=('Descripción'))
    colegio = models.ForeignKey(Colegios, on_delete=models.CASCADE, related_name='quizzesf')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='quizzesf', verbose_name= ("Asignatura"))
    curso = models.ForeignKey(Curso, on_delete= models.CASCADE, related_name='quizzesf')
    lock = models.BooleanField(null= False, default = True, verbose_name=('Bloquear'), help_text="Solo estara disponible para ti el alumno no vera el examen a no ser que desactives esta opción.")
    time = models.IntegerField(verbose_name=('Duración'),help_text="Se deben ingresar en minutos es decir 1 es un minuto, 2 dos minutos y asi.")
    created = models.DateField(auto_now_add=True)


    def __str__(self):
        return self.name


class Questionf(models.Model):
    quiz = models.ForeignKey(QuizFor, on_delete=models.CASCADE, related_name='fquestions')
    text = models.CharField('Pregunta', max_length=255)
    image = models.ImageField( null = True, upload_to='quiz_images',default=False, blank=True,verbose_name=('Imagen'))
    document = models.FileField(null=True, upload_to='quiz_images',default= False, blank=True,verbose_name=('Audio'))
    category = models.ForeignKey(Category,
                                 verbose_name=_("Contenido"),
                                 blank=True,
                                 null=True,
                                 on_delete=models.CASCADE)

    description = models.TextField(max_length=500, null= True, blank=True, verbose_name=("Descripción") )

    def __str__(self):
        return self.text



class Answerf(models.Model):
    question = models.ForeignKey(Questionf, on_delete=models.CASCADE, related_name='fanswers')
    text = models.CharField('Respuesta', max_length=255)
    is_correct = models.BooleanField('Respuesta Correcta', default=False)
    answer_order = models.CharField(
        max_length=30, null=True, blank=True,
        choices=ANSWER_ORDER_OPTIONS,
        help_text=_("El orden en que se muestran al usuario las opciones de respuesta de varias opciones"),
        verbose_name=_("Orden de respuesta"))


    def __str__(self):
        return self.text



class Planificacion(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='planificaciones')
    objetivos_aprendizaje= models.TextField(max_length=1000, verbose_name="Objetivo de aprendizaje")
    curso = models.ForeignKey(Curso , on_delete=models.CASCADE, related_name='planificaciones')
    asignatura = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='planificaciones')
    unidad = models.CharField(max_length=250, verbose_name=('OA'))
    def __str__(self):
        return self.unidad

class PlanificacionIndicador(models.Model):
    """
    Una clase para los indicadores de la planificacion
    """
    objetivo = models.ForeignKey(Planificacion, on_delete=models.CASCADE, related_name='has_indicador')
    indicador_evaluacion = models.CharField(max_length=400, verbose_name="Indicador de evaluación")
    dominio_cognitivo = models.ForeignKey(Dominio, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.indicador_evaluacion

class TextoQuestion(models.Model):

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="texto")
    titulo = models.CharField(max_length=250, blank=True)
    subtitulo = models.CharField(max_length=250, blank=True)
    texto = models.TextField(max_length=5000)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo

class Preguntas(models.Model):
    asignatura = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='asignatura')
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE, related_name='curso')
    titulo = models.CharField('Pregunta', max_length=255)
    
    def __str__(self):
        return self.titulo


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    number = models.IntegerField(verbose_name=("N° de la pregunta"))
    objetivo = models.ForeignKey(Planificacion, on_delete=models.SET_NULL, related_name='questions', verbose_name=('OA'), blank=True, null=True)
    indicador = models.ForeignKey(PlanificacionIndicador, on_delete=models.SET_NULL, related_name='questions', verbose_name=('Indicador Evaluación'), blank=True, null=True)
    text = models.CharField('Pregunta', max_length=255, null= True, blank=True)
    pregunta= models.ForeignKey(Preguntas, on_delete=models.CASCADE, verbose_name=('Banco de preguntas'), help_text='Puedes buscar alguna pregunta aquí', null= True, blank=True)
    texto= models.ForeignKey(TextoQuestion, on_delete=models.CASCADE, blank=True, null=True, verbose_name=('Texto'),help_text='Si tienes algún texto creado para esta prueba seleccionalo si no dejalo en blanco.')
    image = models.ImageField( null = True, upload_to='quiz_images',default=False, blank=True,verbose_name=('Imagen'))
    document = models.FileField(null=True, upload_to='quiz_images',default= False, blank=True,verbose_name=('Audio'))
    video = models.FileField(null=True, upload_to='quiz_images',default= False, blank=True,verbose_name=('Video'))
    url = models.URLField(null=True, blank=True, help_text='Poner un video de YouTube')
    description = models.CharField(max_length=255, null= True, blank=True, verbose_name=("Pregunta 2") )
    puntaje = models.IntegerField(verbose_name=("Puntos"), help_text=("Puntaje que tendra cada pregunta que dara un total para la nota final del alumno."))

    objects = InheritanceManager()



    def __str__(self):
        return self.text



class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    text = models.CharField('Respuesta', max_length=255)
    is_correct = models.BooleanField('Respuesta Correcta', default=False)
    answer_order = models.CharField(
        max_length=30, null=True, blank=True,
        choices=ANSWER_ORDER_OPTIONS,
        help_text=_("El orden en que se muestran al usuario las opciones de respuesta de varias opciones"),
        verbose_name=_("Orden de respuesta"))


    def __str__(self):
        return self.text








class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuiz')
    quizzesf = models.ManyToManyField(QuizFor, through='TakenQuizfor')
    colegio = models.ManyToManyField(Colegios,  related_name='colegio_students')
    interests = models.ManyToManyField(Subject, related_name='interested_students', verbose_name=('Asignaturas'), default=True)
    curso = models.ManyToManyField(Curso ,related_name='curso_students' )
    nivel = models.ManyToManyField(Niveles,related_name='nivel_students')

    def get_unanswered_questions(self, quiz):
        answered_questions = self.quiz_answers \
            .filter(answer__question__quiz=quiz) \
            .values_list('answer__question__pk', flat=True)
        questions = quiz.questions.exclude(pk__in=answered_questions).order_by('id')
        return questions




    class Meta:
        verbose_name = ("Estudiante")

    def __str__(self):
        return self.user.first_name






class Fundacion(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuizF')
    colegio = models.ManyToManyField(Colegios,  related_name='colegio_fundacion')
    interests = models.ManyToManyField(Subject, related_name='interested_fundacion', verbose_name=('Asignaturas'))
    curso = models.ManyToManyField(Curso ,related_name='curso_fundacion' )

    def get_unanswered_questions(self, quiz):
        answered_questions = self.quiz_answers \
            .filter(answer__question__quiz=quiz) \
            .values_list('answer__question__pk', flat=True)
        questions = quiz.questions.exclude(pk__in=answered_questions).order_by('text')
        return questions


    def __str__(self):
        return self.user.username


class Gestion(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuizG')
    colegio = models.ManyToManyField(Colegios, related_name='colegio_gestion')
    interests = models.ManyToManyField(Subject, related_name='interested_gestion', verbose_name=('Asignaturas'))
    curso = models.ManyToManyField(Curso ,related_name='curso_gestion' )

    def __str__(self):
        return self.user.username

class Jefe_Depto(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuizJD')
    colegio = models.ManyToManyField(Colegios,  related_name='colegio_jefedepto')
    interests = models.ManyToManyField(Subject, related_name='interested_jefedepto', verbose_name=('Asignaturas'))
    curso = models.ManyToManyField(Curso ,related_name='curso_jefedepto' )




    def __str__(self):
        return self.user.first_name


class CorreosJefe(models.Model):
    jefe = models.ForeignKey(Jefe_Depto, on_delete=models.CASCADE, related_name='correos')
    correo = models.EmailField()

    def __str__(self):
        return self.correo


class TakenQuiz(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='taken_quizzes')
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='taken_quizzes')
    score = models.FloatField()
    puntaje = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.student.user.first_name


class TakenQuizfor(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='taken_quizzesf')
    quiz = models.ForeignKey(QuizFor, on_delete=models.CASCADE, related_name='taken_quizzesf')
    date = models.DateTimeField(auto_now_add=True)

class TakenQuizJD(models.Model):
    jefedepto = models.ForeignKey(Jefe_Depto, on_delete=models.CASCADE, related_name='taken_quizzesjd')
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='taken_quizzesjd')
    score = models.FloatField()
    date = models.DateField(auto_now_add=True)


class TakenQuizG(models.Model):
    gestion = models.ForeignKey(Gestion, on_delete=models.CASCADE, related_name='taken_quizzesg')
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='taken_quizzesg')
    score = models.FloatField()
    date = models.DateField(auto_now_add=True)

class TakenQuizF(models.Model):
    fundacion = models.ForeignKey(Fundacion, on_delete=models.CASCADE, related_name='taken_quizzesf')
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='taken_quizzesf')
    score = models.FloatField()
    date = models.DateField(auto_now_add=True)


class StudentAnswer(models.Model):
    student = models.ForeignKey(Student ,on_delete=models.CASCADE, related_name='quiz_answers')
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='+', verbose_name=('Respuesta'))

class FundacionAnswer(models.Model):
    fundacion = models.ForeignKey(Fundacion, on_delete=models.CASCADE, related_name='quiz_answers')
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='+', verbose_name=('Respuesta'))

class QA(models.Model):

    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='quiz_info')
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='quiz_info')




