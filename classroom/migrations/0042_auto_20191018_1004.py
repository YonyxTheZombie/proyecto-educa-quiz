# Generated by Django 2.2.3 on 2019-10-18 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0041_niveles'),
    ]

    operations = [
        migrations.AlterField(
            model_name='niveles',
            name='nivel',
            field=models.CharField(max_length=10),
        ),
    ]
