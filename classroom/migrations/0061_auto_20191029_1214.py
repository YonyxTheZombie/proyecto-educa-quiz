# Generated by Django 2.2.3 on 2019-10-29 15:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0060_auto_20191029_1213'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='planificacioni',
        ),
        migrations.AlterField(
            model_name='question',
            name='planificacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='classroom.PlanificacionIndicador', verbose_name='Indicador Evaluación'),
        ),
    ]
