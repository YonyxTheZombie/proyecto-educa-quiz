# Generated by Django 2.2.7 on 2019-11-25 15:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0072_auto_20191125_1252'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='texto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='classroom.TextoQuestion'),
        ),
        migrations.AlterField(
            model_name='question',
            name='planificacion',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='classroom.PlanificacionIndicador', verbose_name='Indicador Evaluación'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='studentanswer',
            name='answer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='classroom.Answer', verbose_name='Respuesta'),
        ),
    ]
