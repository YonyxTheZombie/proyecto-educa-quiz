# Generated by Django 2.2.3 on 2019-10-23 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0044_takenquiz_puntaje'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='nivel',
            field=models.ManyToManyField(related_name='nivel_students', to='classroom.Niveles'),
        ),
    ]
