# Generated by Django 2.2.3 on 2019-09-10 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0017_auto_20190910_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiz',
            name='max_score',
            field=models.IntegerField(help_text='La cantidad de preguntas que tendrá la prueba.', verbose_name='Cantidad de preguntas'),
        ),
    ]
