# Generated by Django 2.2.7 on 2020-01-14 20:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0102_auto_20200114_1706'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='planificacion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='classroom.PlanificacionIndicador', verbose_name='Indicador Evaluación'),
        ),
    ]
