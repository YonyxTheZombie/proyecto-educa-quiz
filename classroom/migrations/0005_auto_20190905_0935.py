# Generated by Django 2.2.3 on 2019-09-05 13:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0004_auto_20190905_0925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiz',
            name='colegio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='quizzes', to='classroom.Colegios'),
        ),
    ]
