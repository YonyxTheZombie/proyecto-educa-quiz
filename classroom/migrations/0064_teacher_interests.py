# Generated by Django 2.2.3 on 2019-10-29 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0063_auto_20191029_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='interests',
            field=models.ManyToManyField(default=True, related_name='interested_teacher', to='classroom.Subject', verbose_name='Asignaturas'),
        ),
    ]
