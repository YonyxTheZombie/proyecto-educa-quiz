from django.contrib import admin
from classroom.models import (Answer, Category, Question, Quiz,  TakenQuiz,Colegios,Curso, Dominio, Fundacion, Gestion, 
                                Jefe_Depto,Niveles, Planificacion, PlanificacionIndicador, Subject, Teacher,User, StudentAnswer,
                                Preguntas )

admin.site.register(Colegios)
admin.site.register(Curso)
admin.site.register(Answer)
admin.site.register(Category)
admin.site.register(Question)
admin.site.register(User)
admin.site.register(Quiz)
admin.site.register(TakenQuiz)
admin.site.register(Teacher)
admin.site.register(Dominio)
admin.site.register(Subject)
admin.site.register(Niveles)
admin.site.register(Jefe_Depto)
admin.site.register(StudentAnswer)
admin.site.register(Preguntas)
