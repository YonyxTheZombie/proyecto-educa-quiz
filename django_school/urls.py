from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django.conf.urls import url

from django.contrib import admin
from classroom.views import classroom, students, teachers, fundacion, gestion, jefedepto

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('classroom.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', classroom.SignUpView.as_view(), name='signup'),
    path('accounts/signup/student/', students.StudentSignUpView.as_view(), name='student_signup'),
    path('accounts/signup/teacher/', teachers.TeacherSignUpView.as_view(), name='teacher_signup'),
    path('accounts/signup/fundacion/', fundacion.FundacionSignUpView.as_view(), name='fundacion_signup'),
    path('accounts/signup/gestion/', gestion.GestionSignUpView.as_view(), name='gestion_signup'),
    path('accounts/signup/jefedepto/', jefedepto.JefeDSignUpView.as_view(), name='jefedepto_signup'),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
